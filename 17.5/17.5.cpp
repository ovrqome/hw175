﻿// 17.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class vector
{
private:
    double x, y, z;
public:
    vector() : x(0), y(0), z(0) {}
    vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    double returnVector()
    {
        return sqrt(x * x + y * y + z * z);
    }
};

class Player
{
private:
    string name;
    int hp;
    int armorclass;
public:
    void damagehp(int ammaount) { hp -= ammaount; }

    void damagearmorclass(int ammaount) { armorclass -= ammaount; }

    string getname() { return name; }
    void setname(string name1) { name = name1; }

    int gethp() { return hp; }
    void sethp(int hp1) { hp = hp1; }

    int getarmorclass() { return armorclass; }
    void setarmorclass(int armorclass1) { armorclass = armorclass1; }
};


int main()
{
    Player a;

    a.setname("Player One");
    a.sethp(100);
    a.setarmorclass(50);
    std::cout << a.getname() << "\n" << a.gethp() << "\n" << a.getarmorclass();
    std::cout << endl;

    vector i(100, 100, 0);
    i.returnVector();
    std::cout << i.returnVector() << "\n";
}